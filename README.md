# Tarefa Backend GO Dev

Desafio simples, composto por etapas significativas para montar um restful api server
---

### Descrição da tarefa

##### Criar um mini aplicativo com três funcionalidades:
- Rota de ping, que responde "pong";
- Rota de CREATE, que recebe um json e o armazena no banco de dados;
- Rota de READ, que retorna o registro armazenado no banco com todos os campos.
- A rota READ deve retornar o registro pelo campo "a"
- No create, apenas "c", "d" e "e" são editáveis e obrigatórios. "a" e "b" são gerados automaticamente pelo banco de dados ou pelo código/algorítmo.


##### Como entregar:
- Os testes referentes aos três endpoints funcionando em CURL, Postman ou Insomnia ( ou "go test" )
- Enviar os fontes por email para __miguel@salu.com.vc__

---
#### Insumos:
Qualquer banco de dados serve. Se for PostgreSql, recomendamos uma conta gratuita em ElephantSQL ( https://customer.elephantsql.com/login )

##### Tabela [t1] para a API REST
- a INT NOT NULL PRIMARY KEY "AUTOINCREMENT"
- b DATE NOT NULL DEFAULT CURRENT_TIMESTAMP
- c VARCHAR(100) NOT NULL
- d NUMERIC(7,2) NOT NULL
- e BOOL




